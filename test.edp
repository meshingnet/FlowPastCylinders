// Parameters
verbosity = 0;
int nn = 1;
real nu = 0.00125; // Re = 400
real dt = 0.05;
real epsv = 1e-6;
real epsu = 1e-6;
real epsp = 1e-6;

//double cylinder Mesh
border a0(t=1, -1){x=-2; y=t; label=1;}
border a1(t=2*pi, 0){x=0.25*(2.+cos(t)/2.); y=0.3+0.25*sin(t)/2.; label=2;}
border a2(t=2*pi, 0){x=0.25*(2.+cos(t)/2.); y=-0.3+0.25*sin(t)/2.; label=2;}

border a3(t=0, 1){x=-2+10*t; y=-1; label=2;}
border a4(t=-1, 1){x=8; y=t; label=3;}
border a5(t=1, 0){x=-2+10*t; y=1; label=4;}

mesh Th = buildmesh(a0(40*nn)+a1(20*nn) +a2(20*nn)+ a3(200*nn) + a4(40*nn) + a5(200*nn));
mesh Tb = Th;
plot(Th,wait=1);
savemesh(Th,"operating/Th.mesh");
savemesh(Tb,"operating/Tb.mesh");
system("cp operating/Tb.mesh.gmsh ./");
system("cp operating/Tb.mesh.gmsh operating/mesh/");
system("cp operating/Tb.mesh operating/mesh/0.mesh");
system("cd filter;./dapro;cd../");

// Fespace
fespace Vh(Th, P1);
Vh w;
Vh u=0, v=0;
Vh p=0;
Vh q=0;

// Definition of Matrix dtMx and dtMy
matrix dtM1x, dtM1y;

// Macro
macro BuildMat()
{   /* for memory managenemt */
    varf vM(unused, v) = int2d(Th)(v);
    varf vdx(u, v) = int2d(Th)(v*dx(u)*dt);
    varf vdy(u, v) = int2d(Th)(v*dy(u)*dt);

    real[int] Mlump = vM(0, Vh);
    real[int] one(Vh.ndof); one = 1;
    real[int] M1 = one ./ Mlump;
    matrix dM1 = M1;
    matrix Mdx = vdx(Vh, Vh);
    matrix Mdy = vdy(Vh, Vh);
    dtM1x = dM1*Mdx;
    dtM1y = dM1*Mdy;
} //

// Build matrices
BuildMat

// Time iterations
real err = 1.;
real outflux = 1.;
for(int n = 0; n < 301; n++){

    cout<< "CPU time start= " << (clock()) <<endl;
    // Update
    Vh uold=u, vold=v, pold=p;

    // Solve
    solve pb4u (u, w, init=n, solver=CG, eps=epsu)
        = int2d(Th)(
              u*w/dt
            + nu*(dx(u)*dx(w) + dy(u)*dy(w))
        )
        -int2d(Th)(
                convect([uold, vold], -dt, uold)/dt*w
            - dx(p)*w
        )
        + on(1, u=1*y*(-y)+1)
        + on(2, 4, u=0)
        ;


    solve pb4v (v, w, init=n, solver=CG, eps=epsv)
        = int2d(Th)(
              v*w/dt
            + nu*(dx(v)*dx(w) + dy(v)*dy(w))
        )
        -int2d(Th)(
                convect([uold,vold],-dt,vold)/dt*w
            - dy(p)*w
        )
        +on(1, 2, 3, 4, v=0)
        ;

    solve pb4p (q, w, solver=CG, init=n, eps=epsp)
        = int2d(Th)(
              dx(q)*dx(w)+dy(q)*dy(w)
        )
        - int2d(Th)(
              (dx(u)+ dy(v))*w/dt
        )
        + on(3, q=0)
        ;

    //to have absolute epsilon in CG algorithm.
    epsv = -abs(epsv);
    epsu = -abs(epsu);
    epsp = -abs(epsp);

    p = pold-q;
    u[] += dtM1x*q[];
    v[] += dtM1y*q[];

    // Error & Outflux
    err = sqrt(int2d(Th)(square(u-uold)+square(v-vold))/Th.area);
    outflux = int1d(Th)([u,v]'*[N.x,N.y]);
    cout << " iter " << n << " Err L2 = " << err << " outflux = " << outflux << endl;

// for error estimate

string string1 = "operating/u/"+n+".txt" ;
string string2 = "operating/v/"+n+".txt" ;
{
	ofstream f(string1);
	f << u[] ;
};
{
	ofstream f(string2);
	f << v[] ;
};
{
	ofstream f("operating/clock.txt");
	f << n;
};
    cout<< "CPU time end FEM= " << (clock()) <<endl;

	system("cd operating;./transfer;cd ../");
    cout<< "CPU time end mesh adapt= " << (clock()) <<endl;

    // Mesh adaptation

	string string0 = "operating/mesh/"+(n+1)+".mesh" ;
        Th = readmesh(string0);
        BuildMat // Rebuild mat.
	plot(Th);
    cout<< "CPU time end read mesh= " << (clock()) <<endl;
}
cout<<"generating NN"<<endl;
