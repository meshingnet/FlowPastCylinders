#Import packages
from tensorflow import __version__ as tf_version, float32 as tf_float32, Variable
from tensorflow.keras import Sequential, Model
from tensorflow.keras.backend import variable, dot as k_dot, sigmoid, relu
from tensorflow.keras.layers import Dense, Input, Concatenate, Layer
from numpy import __version__ as np_version, unique, array, mean, argmax
import numpy
import tensorflow as tf
import scipy.sparse as sp

from pycode.filters import normalized_laplacian, rescale_laplacian, chebyshev_polynomial
from pycode.multi_graph_cnn_layer import MultiGraphCNN
from pycode.norm import norm, reverse_norm, trainnorm
from pycode.filter_cast import filtercast
from pycode.post import postprocess
from pycode.module import gcnmode

import time

# Load data n1 for training
n1=37392 
#sequence length
nn=500

# compute the filters
nfilters=2

row = numpy.genfromtxt("../data/row.csv",dtype=numpy.int32)
col = numpy.genfromtxt("../data/col.csv",dtype=numpy.int32)
data = numpy.ones(row.shape)

adj = sp.coo_matrix((data,(row,col)),shape=(n1,n1), dtype=numpy.float32)

print('Using Chebyshev polynomial basis filters...')
A = filtercast(adj,nfilters)
print("A is :")
print(A)
time.sleep(10)

