import tensorflow as tf
from pycode.multi_graph_cnn_layer import MultiGraphCNN
from tensorflow.keras.layers import Dense, Input, Concatenate, Layer
from tensorflow.keras import Sequential, Model


def gcnmode(X,A,nfilters):

    X_shape = Input(shape=(X.shape[1],X.shape[2]))
    gc1_layer = MultiGraphCNN(output_dim=8, num_filters=nfilters,filters=A, activation='relu')(X_shape)
    gc2_layer = MultiGraphCNN(output_dim=16, num_filters=nfilters,filters=A,activation='relu')(gc1_layer)
    gc3_layer = MultiGraphCNN(output_dim=32, num_filters=nfilters,filters=A, activation='relu')(gc2_layer)
    gc4_layer = MultiGraphCNN(output_dim=16, num_filters=nfilters,filters=A, activation='relu')(gc3_layer)
    gc5_layer = MultiGraphCNN(output_dim=8, num_filters=nfilters,filters=A,activation='relu')(gc4_layer)
    gc6_layer = MultiGraphCNN(output_dim=3, num_filters=nfilters,filters=A, activation='relu')(gc5_layer)

    model3 = Model(inputs=X_shape, outputs=gc6_layer, name="Model_3")

    return model3
