#Import packages
from tensorflow import __version__ as tf_version, float32 as tf_float32, Variable
from tensorflow.keras import Sequential, Model
from tensorflow.keras.backend import variable, dot as k_dot, sigmoid, relu
from tensorflow.keras.layers import Dense, Input, Concatenate, Layer
from tensorflow.keras.utils import plot_model
from numpy import __version__ as np_version, unique, array, mean, argmax
from filters import normalized_laplacian, rescale_laplacian, chebyshev_polynomial
import numpy
import tensorflow as tf
import scipy.sparse as sp

print("tensorflow version:", tf_version)
print("numpy version:", np_version)

from multi_graph_cnn_layer import MultiGraphCNN
from norm import norm, reverse_norm
from filter_cast import filtercast
from post import postprocess
from module import gcnmode

# Load data n1 for training
n1=4458 
n2=4458
#sequence length
nn=1
'''
character1 = numpy.loadtxt("../data/total.am",delimiter=",")
character1 = character1.reshape((nn,n1,3))
'''
character2 = numpy.loadtxt("../character.txt",delimiter=",")
character2 = character2.reshape((nn,n1,6))

#normalisation
#character1 = norm(character1)
character2 = norm(character2)
'''
X1_1 = character1[0:nn-10,0:n1,0:3]
X1_2 = character1[5:nn-5,0:n1,0:3]
X2_1 = character2[0:nn-10,0:n1,0:3]
X2_2 = character2[5:nn-5,0:n1,0:3]

X1 = tf.concat([X1_1,X1_2],axis=2)
X2 = tf.concat([X2_1,X2_2],axis=2)
X = X1

Y1 = character1[10:nn,0:n1,0]
Y2 = character1[10:nn,0:n1,1]
Y3 = character1[10:nn,0:n1,2]
output_data = character1[10:nn,0:n1,0:3]
'''
# compute the filters
nfilters=2

row = numpy.genfromtxt("../data/row.csv",dtype=numpy.int32)
col = numpy.genfromtxt("../data/col.csv",dtype=numpy.int32)
data = numpy.ones(row.shape) 
adj = sp.coo_matrix((data,(row,col)),shape=(n2,n2), dtype=numpy.float32)

print('Using Chebyshev polynomial basis filters...')
B = filtercast(adj,nfilters)

# output_data = output_data.reshape((300,n2))

# Check dimensions
#print("Features matrix dimension:", X.shape, "| output array dimension:", output_data.shape, "| Adjacency matrix dimension:", A.shape)
'''
#output_classes = len(unique(labels))
iterations = 500
#batch_size = X.shape[0]

# Model 3: neural network with graph convolutional layer

#model3 = gcnmode(X,A,nfilters)

#model3.save_weights("model3_initial_weights.h5")

#model3.summary()
#plot_model(model3, 'model3.png', show_shapes=True)

# setting training
#acc_model3 = []

#model3.load_weights("model3_initial_weights.h5")
model3.compile(
optimizer='adam',
loss='mse',
metrics=['accuracy']
)

# training the model
#model3.fit(X, output_data, epochs=2000, batch_size=8, shuffle=False, verbose=1)
#model3.save_weights("model3_weights1.h5")
'''
#construct model4 for testing
'''
test1 = character2[0:nn-10,0:n2,0:3]
test2 = character2[5:nn-5,0:n2,0:3]
test = tf.concat([test1,test2],axis=2)
'''
model4 = gcnmode(character2,B,nfilters)
model4.load_weights("model3_weights1.h5")

#testing phase
character2 = numpy.loadtxt("../character.txt",delimiter=",")
character2 = character2.reshape((nn,n2,6))
character2 = norm(character2)
'''
test1 = character2[0:nn-10,0:n2,0:3]
test2 = character2[5:nn-5,0:n2,0:3]
test = tf.concat([test1,test2],axis=2)
#truth = character2[10:nn,0:n2,0:3].reshape(((nn-10),n2,3))
input2 = character2[5:nn-5,0:n2,0:3].reshape(((nn-10),n2,3))
character2 = numpy.loadtxt("../character.txt",delimiter=",")
character2 = character2.reshape((nn,n2,6))
'''
predictions = model4.predict(character2)
predictions=predictions.reshape((nn,n2,3))

predictions=reverse_norm(predictions)
predictions = predictions.reshape((n2*3))
numpy.savetxt("predictions.txt",predictions,fmt="%s",delimiter=" ")
'''
truth = reverse_norm(character2,truth)
input2 = reverse_norm(character2,input2)
postprocess(predictions,truth,input2)
'''
