import numpy
import os

def postprocess(predictions,truth,inputs):
    n0 = predictions.shape[0]
    n1 = predictions.shape[1]
    n2 = predictions.shape[2]

    predictions0 = predictions[0:n0,0:n1,0]
    predictions1 = predictions[0:n0,0:n1,1]
    predictions2 = predictions[0:n0,0:n1,2]

    truth0 = truth[0:n0,0:n1,0]
    truth1 = truth[0:n0,0:n1,1]
    truth2 = truth[0:n0,0:n1,2]

    inputs0 = inputs[0:n0,0:n1,0]
    inputs1 = inputs[0:n0,0:n1,1]
    inputs2 = inputs[0:n0,0:n1,2]

    predictions = predictions.reshape((n0*n1*n2))
    predictions0 = predictions0.reshape((n0*n1))
    predictions1 = predictions1.reshape((n0*n1))
    predictions2 = predictions2.reshape((n0*n1))

    truth = truth.reshape((n0*n1*n2))
    truth0 = truth0.reshape((n0*n1))
    truth1 = truth1.reshape((n0*n1))
    truth2 = truth2.reshape((n0*n1))

    inputs = inputs.reshape((n0*n1*n2))
    inputs0 = inputs0.reshape((n0*n1))
    inputs1 = inputs1.reshape((n0*n1))
    inputs2 = inputs2.reshape((n0*n1))

    numpy.savetxt("predictions.txt",truth0,fmt="%s",delimiter=" ")
    os.system('./paraview')
    #evaluate
    errorp=(truth-predictions)
    errorp=numpy.dot(errorp,errorp)
    errorp=numpy.sum(errorp)
    print("predictions error")
    print(errorp)

    errori=(truth-inputs)
    errori=numpy.dot(errori,errori)
    errori=numpy.sum(errori)
    print("input error")
    print(errori)

    print("ratio")
    print(errorp/errori)

    return
