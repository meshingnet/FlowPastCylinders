import numpy as np
from scipy import sparse
import scipy.sparse as sp
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
from scipy.sparse.linalg.eigen.arpack import eigsh, ArpackNoConvergence
def preprocess_adj(adj, symmetric=True):
    adj = adj + sp.eye(adj.shape[0])
    adj = normalize_adj(adj, symmetric)
    return adj

def normalize_adj(adj, symmetric=True):
    if symmetric:
        d = sp.diags(np.power(np.array(adj.sum(1)), -0.5).flatten(), 0)
        a_norm = adj.dot(d).transpose().dot(d)
        a_norm = sparse.csr_matrix(a_norm)
    else:
        d = sp.diags(np.power(np.array(adj.sum(1)), -1).flatten(), 0)
        a_norm = d.dot(adj).tocsr()
    return a_norm

def normalized_laplacian(adj, symmetric=True):
    adj_normalized = normalize_adj(adj, symmetric)
    laplacian = sp.eye(adj.shape[0]) - adj_normalized
    return laplacian

def rescale_laplacian(laplacian):
    try:
        print('Calculating largest eigenvalue of normalized graph Laplacian...')
        largest_eigval = eigsh(laplacian, 1, which='LM', return_eigenvectors=False)[0]
    except ArpackNoConvergence:
        print('Eigenvalue calculation did not converge! Using largest_eigval=2 instead.')
        largest_eigval = 2

    scaled_laplacian = (2. / largest_eigval) * laplacian - sp.eye(laplacian.shape[0])
    return scaled_laplacian

def chebyshev_polynomial(X, k):
    """Calculate Chebyshev polynomials up to order k. Return a list of sparse matrices."""
    print("Calculating Chebyshev polynomials up to order {}...".format(k))
    '''
    T_k = list()
#    T_k.append(sp.eye(X.shape[0]).tocsr())
#    T_k.append(X)
    T_k.append(sp.eye(X.shape[0]).tocsr().toarray())
    T_k.append(X.toarray())

    def chebyshev_recurrence(T_k_minus_one, T_k_minus_two, X):
        X_ = sp.csr_matrix(X, copy=True)
        return 2 * X_.dot(T_k_minus_one) - T_k_minus_two

    for i in range(2, k+1):
        #T_k.append(chebyshev_recurrence(T_k[-1], T_k[-2], X))
        T_k.append(chebyshev_recurrence(T_k[-1], T_k[-2], X))


    return T_k

'''
    T_k=sp.coo_matrix(sp.vstack((sp.eye(X.shape[0]),X)))
 #   T_k.append(sp.eye(X.shape[0]).tocsr().toarray())
 #   T_k.append(X.toarray())

    def chebyshev_recurrence(T_k_minus_one, T_k_minus_two, X):
        X_ = sp.coo_matrix(X, copy=True)
        return sp.coo_matrix((2 * X_.dot(T_k_minus_one) - T_k_minus_two),copy=True)

    for i in range(2, k+1):
        #T_k.append(chebyshev_recurrence(T_k[-1], T_k[-2], X))
        T_k=sp.vstack( (T_k,chebyshev_recurrence(T_k[-1], T_k[-2], X)) )

    return T_k
