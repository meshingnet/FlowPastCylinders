import numpy

def norm(character):
        #sequence number
        n0 = character.shape[0]
        #order of the graph
        n1 = character.shape[1]
        #character channel
        n2 = character.shape[2]

        maximum = numpy.loadtxt("../operating/max.txt")
        minimum = numpy.loadtxt("../operating/min.txt")

        minimum = minimum.reshape((1,n1,3))
        minimum = numpy.concatenate((minimum,minimum),axis=2)
        maximum = maximum.reshape((1,n1,3))
        maximum = numpy.concatenate((maximum,maximum),axis=2)
        diff = maximum - minimum

        for i in range (n1):
            for j in range (n2):
                if (diff[0,i,j]==0):
                    diff[0,i,j]=1

        character = numpy.divide((character-minimum),diff)
        return (character)

def reverse_norm(predictions):
        n0 = predictions.shape[0]
        n1 = predictions.shape[1]
        n2 = predictions.shape[2]

        maximum = numpy.loadtxt("../operating/max.txt")
        minimum = numpy.loadtxt("../operating/min.txt")

        minimum = minimum.reshape((1,n1,n2))
        maximum = maximum.reshape((1,n1,n2))
        diff = maximum - minimum

        for i in range (n1):
            for j in range (n2):
                if (diff[0,i,j]==0):
                    diff[0,i,j]=1

        predictions = numpy.multiply(predictions,(maximum-minimum))+minimum
        return predictions

def trainnorm(character):
        #sequence number
        n0 = character.shape[0]
        #order of the graph
        n1 = character.shape[1]
        #character channel
        n2 = character.shape[2]

        maximum = numpy.loadtxt("../operating/max.txt")
        minimum = numpy.loadtxt("../operating/min.txt")

        minimum = minimum.reshape((1,n1,3))
        maximum = maximum.reshape((1,n1,3))
        mini = minimum
        maxi = maximum

        for i in range (n0-1):
            minimum = numpy.concatenate((minimum,mini),axis=0)
            maximum = numpy.concatenate((maximum,maxi),axis=0)

        print(character.shape)
        print('max shape')
        print(maximum.shape)

        diff = maximum - minimum

        print('diff shape')
        print(diff.shape)
        for i in range (n1):
            for j in range (n2):
                for k in range (n0):
                    if (diff[k,i,j]==0):
                        diff[k,i,j]=1

        character = numpy.divide((character-minimum),diff)
        return (character)

