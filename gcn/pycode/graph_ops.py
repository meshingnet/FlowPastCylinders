import keras.backend as K
import tensorflow as tf

def graph_conv_op(x, num_filters, graph_conv_filters,kernel):
    x=tf.cast(x, tf.float32)
    graph_conv_filters=tf.cast(graph_conv_filters, tf.float32)
    kernel=tf.cast(kernel, tf.float32)

    if len(x.get_shape()) == 2:
        conv_op = K.dot(graph_conv_filters, x)
        conv_op = tf.split(conv_op, num_filters, axis=0)
        conv_op = K.concatenate(conv_op, axis=1)
    elif len(x.get_shape()) == 3:
#        composite = x[0,:,:]
#        condot = K.dot(graph_conv_filters,composite) 
#        for i in range(1,basize):
#            composite = x[i,:,:]
#            comdot = K.dot(graph_conv_filters,composite) 
#            condot = K.concatenate((condot,comdot),axis = 0)
#        conv_op = tf.reshape(condot,[basize,9933,2])
        numfeature=x.shape.as_list()[2]
        numnode=x.shape.as_list()[1]
        x = tf.transpose(x,perm=[1,0,2])


        filters = graph_conv_filters
        feature = x[0:numnode,:,0]
        conv_op = tf.sparse.sparse_dense_matmul(filters,feature)
        conv_op = tf.expand_dims(conv_op,axis=2)

        for i in range (numfeature-1):
            #feature 3311,300 filters 9933 3311
            feature = x[0:numnode,:,i+1]
            #component 9933 300 1
            component = tf.sparse.sparse_dense_matmul(filters,feature)
            component = tf.expand_dims(component,axis=2)
            #conv_op 9933,300,2
            conv_op = K.concatenate((conv_op,component),axis=2)
        
        conv_op = tf.split(conv_op, num_filters, axis=0)
        conv_op = K.concatenate(conv_op, axis=2)
        conv_op = tf.transpose(conv_op,perm=[1,0,2])

    else:
        raise ValueError('x must be either 2 or 3 dimension tensor'
                         'Got input shape: ' + str(x.get_shape()))
    conv_out = K.dot(conv_op, kernel)
    return conv_out
