from pycode.filters import normalized_laplacian, rescale_laplacian, chebyshev_polynomial
import numpy
import tensorflow as tf
'''
def filtercast(adj,nfilters):
    L = normalized_laplacian(adj,symmetric=True)
    L_scaled = rescale_laplacian(L)
    A = chebyshev_polynomial(L_scaled, nfilters-1)
    A = numpy.concatenate(A,axis=0)
    A = tf.sparse.from_dense(A)
    print('a is')
    print(A)

    return A
'''
def filtercast(adj,nfilters):
    L = normalized_laplacian(adj,symmetric=True)
    L_scaled = rescale_laplacian(L)
    A = chebyshev_polynomial(L_scaled, nfilters-1)

    print('nfilter = ')
    print(nfilters)
    print('a is')
    print(A)
    
    A = tf.SparseTensor(
    indices=numpy.array([A.row, A.col]).T,
    values=A.data,
    dense_shape=A.shape)

    return A

