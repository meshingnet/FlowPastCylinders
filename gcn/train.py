from tensorflow import __version__ as tf_version, float32 as tf_float32, Variable
from tensorflow.keras import Sequential, Model
from tensorflow.keras.backend import variable, dot as k_dot, sigmoid, relu
from tensorflow.keras.layers import Dense, Input, Concatenate, Layer
from numpy import __version__ as np_version, unique, array, mean, argmax
import numpy
import tensorflow as tf
import scipy.sparse as sp

from pycode.filters import normalized_laplacian, rescale_laplacian, chebyshev_polynomial
from pycode.multi_graph_cnn_layer import MultiGraphCNN
from pycode.norm import norm, reverse_norm, trainnorm
from pycode.filter_cast import filtercast
from pycode.post import postprocess
from pycode.module import gcnmode

import time

import tensorflow as tf
print(tf.__version__)

# Load data n1 for training (vertices nb of background mesh)

#sequence length
nn=500

character2 = numpy.loadtxt("../operating/traindata/2.txt")
print (character2.shape)
n1=int(character2.shape[0]/nn)
character2 = character2.reshape((nn,n1,3))

#normalisation
character = trainnorm(character2)

X1 = character[0:nn-2,0:n1,0:3]
X2 = character[1:nn-1,0:n1,0:3]
X = tf.concat([X1,X2],axis=2)
print('intput is:')
print(X)

output_data = character[2:nn,0:n1,0:3]

time.sleep(10)
# compute the filters
nfilters=2

row = numpy.genfromtxt("../data/row.csv",dtype=numpy.int32)
col = numpy.genfromtxt("../data/col.csv",dtype=numpy.int32)
data = numpy.ones(row.shape)

adj = sp.coo_matrix((data,(row,col)),shape=(n1,n1), dtype=numpy.float32)

print('Using Chebyshev polynomial basis filters...')
A = filtercast(adj,nfilters)
print("A is :")
print(A)

print("x.shape")
print(X.shape)

iterations = 500
batch_size = X.shape[0]

# Model 3: neural network with graph convolutional layer

model3 = gcnmode(X,A,nfilters)
#model3.load_weights("model/2.h5")

model3.summary()

model3.compile(
optimizer='adam',
loss='mse',
metrics=['accuracy']
)
# training the model
model3.fit(X, output_data, epochs=3000, batch_size=32, shuffle=False, verbose=1)
model3.save_weights("model/2.h5")
