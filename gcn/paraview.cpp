#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<iomanip>
#include<string>
using namespace std;

int nvertex, nele, nedge, i, label, v1, v2, v3;
int loop;
double x, y, character;
char name[100];
string line;
int main()
{
	ifstream incha("predictions.txt");
	for(loop=0;loop<490;loop++)
	{

	ifstream inmesh("../data/Th.mesh");
	sprintf(name,"predictions/predictions_%d.vtk",loop);
	ofstream out(name);
	while(line!="Vertices"){
	getline(inmesh,line);
	}
	inmesh>>nvertex;
	out<<"# vtk DataFile Version 2.0"<<endl;
	out<<"Prediction data"<<endl;
	out<<"ASCII"<<endl;
	out<<"DATASET UNSTRUCTURED_GRID"<<endl;
	out<<"POINTS "<<nvertex<<" float"<<endl;
	for(i=0;i<nvertex;i++)
	{
		inmesh>>x>>y>>label;
		out<<x<<" "<<y<<" 0.0"<<endl;
	}
	while(line!="Triangles"){
	getline(inmesh,line);
	}
	inmesh>>nele;
	out<<endl<<"CELLS "<<nele<<" "<<nele*4<<endl;
	for(i=0;i<nele;i++)
	{
		inmesh>>v1>>v2>>v3>>label;
		out<<"3 "<<v1-1<<" "<<v2-1<<" "<<v3-1<<endl;
	}
	out<<endl<<"CELL_TYPES "<<nele<<endl;
	for(i=0;i<nele;i++)
	{
		out<<5<<endl;
	}
	out<<endl<<"POINT_DATA "<<nvertex<<endl;
	out<<"SCALARS velocity_y float 1"<<endl;
	out<<"LOOKUP_TABLE default"<<endl;
	for(i=0;i<nvertex;i++)
	{
		incha>>character;
		out<<character<<endl;
	}

	out.close();
	inmesh.close();
	}
	return 0;
}

