#Import packages
from tensorflow import __version__ as tf_version, float32 as tf_float32, Variable
from tensorflow.keras import Sequential, Model
from tensorflow.keras.backend import variable, dot as k_dot, sigmoid, relu
from tensorflow.keras.layers import Dense, Input, Concatenate, Layer
from tensorflow.keras.utils import plot_model
from numpy import __version__ as np_version, unique, array, mean, argmax
import numpy
import tensorflow as tf
import scipy.sparse as sp

print("tensorflow version:", tf_version)
print("numpy version:", np_version)

from pycode.filters import normalized_laplacian, rescale_laplacian, chebyshev_polynomial
from pycode.multi_graph_cnn_layer import MultiGraphCNN
from pycode.norm import norm, reverse_norm
from pycode.filter_cast import filtercast
from pycode.post import postprocess
from pycode.module import gcnmode

nn=1 
character2 = numpy.loadtxt("../operating/character.txt",delimiter=",")
n1 = int(character2.shape[0]/2)
print(character2.shape)
character2 = character2.reshape((nn,n1,6))
character2 = norm(character2)

nfilters = 2
row = numpy.genfromtxt("../filter/row.csv",dtype=numpy.int32)
col = numpy.genfromtxt("../filter/col.csv",dtype=numpy.int32)
data = numpy.ones(row.shape) 
adj = sp.coo_matrix((data,(row,col)),shape=(n1,n1), dtype=numpy.float32)

print('Using Chebyshev polynomial basis filters...')
A = filtercast(adj,nfilters)

model4 = gcnmode(character2,A,nfilters)
model4.load_weights("model/5.h5")

character2 = numpy.loadtxt("../operating/character.txt",delimiter=",")
character2 = character2.reshape((nn,n1,6))
character2 = norm(character2)
predictions = model4.predict(character2)
predictions=predictions.reshape((nn,n1,3))

predictions=reverse_norm(predictions)
predictions = predictions.reshape((n1*3))
numpy.savetxt("predictions.txt",predictions,fmt="%s",delimiter=" ")


