This is the organised and highly packaged version

test.edp allows the network to use NN to generate a sequence of meshes during simulation.
The NN model is load from gcn/model.

train.edp first use conventional Hessian error indicator to refine the mesh over the simulation.
The metric sequence makes data for training the network.
Then the network is trained and the weight is saved.
