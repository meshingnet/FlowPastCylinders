//convert Tb.mesh to sparse laplacian matrix in order to compute the filters
#include<fstream>
#include<iostream>
#include<string>
#include<stdlib.h>
#include<iomanip>

using namespace std;

int main()
{
	int nvertex,nelement,nedge;
	string line;

	double temp;
	float u,v,p;
	int v1,v2,v3;
	int i,j;
	int** adj = new int*[39999];
	for(i=0;i<39999;i++)
	{
		adj[i] = new int[39999];
	}
	
	for(i=0;i<39999;i++)
	{
		for(j=0;j<39999;j++)
		{
			adj[i][j]=0;
		}
	}

	ifstream inmesh("../operating/Tb.mesh");
	while(line!="Vertices"){
	getline(inmesh,line);
	}
	inmesh>>nvertex;
	cout<<nvertex;
	for(i=0;i<nvertex;i++)
	{
		inmesh>>temp>>temp>>temp;
	}

	while(line!="Triangles"){
	getline(inmesh,line);
	}
	inmesh>>nelement;

	for(i=0;i<nelement;i++)
	{
		inmesh>>v1>>v2>>v3>>temp;
		adj[v1][v2]=1;
		adj[v2][v1]=1;
		adj[v1][v3]=1;
		adj[v3][v1]=1;
		adj[v2][v3]=1;
		adj[v3][v2]=1;
	}
	ofstream outrow("row.csv");
	ofstream outcol("col.csv");
	for(i=0;i<nvertex;i++)
	{
		for(j=0;j<nvertex;j++)
		{
			if(adj[i][j]==1)
			{
				outrow<<i<<" ";
				outcol<<j<<" ";
			}
		}
	}

	outrow.close();
	outcol.close();
	
	/*
	for(i=0;i<nedge;i++)
	{
		inmesh>>temp>>temp>>temp;
	}
	inmesh.close();
	ofstream outadj("adj.csv");
	for(i=1;i<nvertex+1;i++)
	{
		for(j=1;j<nvertex;j++)
		{
			outadj<<adj[i][j]<<",";
		}
		outadj<<adj[i][nvertex]<<endl;
	}
	outadj.close();
	ifstream inu("u05.txt");
	ifstream inv("v05.txt");
	ifstream inp("p05.txt");
	ofstream outcha("character05.csv");
	for(i=0;i<500;i++)
	{
		inu>>temp;
		inv>>temp;//read vertices number
		inp>>temp;
		for(j=0;j<nvertex;j++)
		{
			inu>>u;
			inv>>v;
			inp>>p;
			outcha<<u<<","<<v<<","<<p<<endl;
		}
		
	}
	inu.close();
	inv.close();
	inp.close();
	outcha.close();
*/
	return 0;
}

